# [Special] Koşaner Sözlükçe Tarama Betiği (K. Lexicon Scanner Script)

Erdin Eray, 2015
<eraygezer.94@gmail.com>

KST is a little script for a bunch of lexemes to search in Google Images API. It does not have any licenses. Feel free to use.

# Contains
 - python3-requests
 - lexscanner (spec)

**Lexscanner:** Lexscanner is a special library for project KSK. It has a sample Google API algorithm inside.
