"""
Lexscanner
Dokuz Eylül Üniversitesi Edebiyat Fakültesi Dilbilim Bölümü Öğretim Görevlisi Özgür Koşaner için
yardımcı olması amacıyla hazırlanmıştır. Özel olarak açık kaynak geliştirilmeyecektir.

Yazar: Eray Erdin <eraygezer.94@gmail.com>
Tarih: 02.2015
Sürüm: 0.1a
Lisans: Yok
"""

import requests
import json, re, urllib.request, datetime, os, logging, time

LOGGER = logging.getLogger('lexscanner_log')
LOGGER.setLevel(logging.DEBUG)

try:
    LOGFILE = logging.FileHandler('{}/.temp/python3/{}.log'.format(os.environ['HOME'], str(datetime.datetime.now())))
except FileNotFoundError:
    if os.path.exists(os.environ['HOME']+'/.temp') != True:
        os.makedirs(os.environ['HOME']+'/.temp/python3')
    LOGFILE = logging.FileHandler('{}/.temp/python3/{}.log'.format(os.environ['HOME'], str(datetime.datetime.now())))
LOGFILE.setLevel(logging.DEBUG)

LOGSTREAM_ERROR = logging.StreamHandler()
LOGSTREAM_ERROR.setLevel(logging.ERROR)

LOGSTREAM_INFO = logging.StreamHandler()
LOGSTREAM_INFO.setLevel(logging.INFO)

LOG_FORMAT = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
LOGFILE.setFormatter(LOG_FORMAT)
LOGSTREAM_ERROR.setFormatter(LOG_FORMAT)
LOGSTREAM_INFO.setFormatter(LOG_FORMAT)

LOGGER.addHandler(LOGSTREAM_ERROR)
LOGGER.addHandler(LOGFILE)
LOGGER.addHandler(LOGSTREAM_INFO)

class Client(object):
    api = 'https://ajax.googleapis.com/ajax/services/search/images?v=1.0'
    a = {
         'q' : '&q={0}',
         'hl' : '&hl={0}',
         'ft' : '&as_filetype={0}',
         'sz' : '&imgsz={0}',
         }
    
    def __init__(self, lexes):
        self.lexes = lexes
    
    def geturls(self, hl = 'tr', ft = None):
        for lex in self.lexes:
            LOGGER.debug('Setting retriever...')
            retriever = self.api+self.a['hl'].format(hl)+self.a['q'].format(lex.lex)
            if ft != None: retriever += self.a['ft'].format(ft)
            LOGGER.debug('Gathering information from Google Images API...')
            while True:
                try:
                    result = json.loads(requests.get(retriever).text)['responseData']['results']
                except TypeError as t:
                    LOGGER.error('Suspected service abusement error. Forcing to try.')
                    continue
                except requests.exceptions.SSLError as SSLError:
                    LOGGER.error('SSL cryption exception raised. Forcing to try.')
                    continue
                break
                LOGGER.debug('Setting image URL container...')
            lex.url = []
            for res in result:
                LOGGER.info('Adding to container: {}\t|\t{}\t|\t{}'.format(lex.id, lex.lex, res['unescapedUrl']))
                lex.url.append(res['unescapedUrl'])
    
    def extract(self, path):
        LOGGER.debug('Checking path string correctness...')
        if path[-1] != '/':
            if path == '': pass
            else: path+='/'
        
        LOGGER.debug('Checking out path existence...')
        if os.path.exists(path) != True:
            os.makedirs(path)
        
        
        for lex in self.lexes:
            for url in lex.url:
                try:
                    LOGGER.info('Downloading image: {}\t|\t{}\t|\t{}'.format(lex.id, lex.lex, url))
                    urllib.request.urlretrieve(url, '{path}{id} {lex} [{index}]{format}'.format(path=path, index=str(lex.url.index(url)), id = lex.id, lex = lex.lex, format = re.findall('\.[a-z][a-z][a-z]', url)[-1]))
                except urllib.error.HTTPError as HTTPError:
                    LOGGER.error(str(HTTPError))
                except urllib.error.URLError as URLError:
                    LOGGER.error(str(URLError))

class Lexeme(object):
    def __init__(self, **kwargs):
        self.__dict__ = kwargs
        